//Write a recursive function that finds the largest number in a given list of
//integers.

// Created by Neil on 11/04/2017.

#include <stdio.h>

int getLargest(int array[], int currentLargest, int size);

int main(void)
{
    int arraySize;
    int largest;


    printf("This program finds the largest number in the given list of integer numbers.\n");

    printf("\n");

    printf("How many numbers would you like to enter ?\n");
    while(scanf("%d", &arraySize) != 1)
    {
        printf("Re-enter a valid size please:\n");
    }

    int array[arraySize];

    for(int i = 0; i < arraySize; i++)
    {
        printf("Enter element %d:\n", i+1);

        while(scanf("%d", &array[i]) != 1)
        {
            printf("Re-enter element %d:\n", i+1);
        }
    }

    printf("\n");

    printf("The list of numbers you entered is: \n");
    for(int i = 0; i < arraySize; i++)
    {
        printf(" %d ", array[i]);
    }

    printf("\n\n");

    largest = getLargest(array, array[0], arraySize-1);
    printf("The largest number is: %d \n", largest);


}

int getLargest(int array[], int currentLargest, int size)
{
    //if there's only one element, then return that element
    //Base case
    if (size == 0)
    {
        return currentLargest;
    }

    //if size is positive
    if(size > -1)
    {
        //if the last element is larger than the current largest
        if(array[size] > currentLargest)
        {
            currentLargest = array[size];
        }

        //always passing a smaller size as an argument, until the base case (size = 1) is reached
        currentLargest = getLargest(array, currentLargest, size-1);

        return currentLargest;
    }
    else
    {
        return currentLargest;
    }
}