//Write a program that, given an array of integers, finds all integers in the
//array that are repeated more than once. Try to find the fastest and most
//memory-efficient way of doing this.

// Created by Neil on 06/04/2017.
//

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

struct returnType
{
    int *dups;
    int size;
};

struct returnType returnDuplic(int *arrayPointer, int arrayLength);

int main(void)
{
    struct returnType funcRet;

    int amount = 0;

    printf("This program will find the repeated integers in a given array.\n");
    printf("\n");

    printf("How many integers would you like to enter?\n");
    while(scanf("%d", &amount) != 1)
    {
        printf("Re enter amount: \n");
    }

    int *array = (int*) malloc(amount * sizeof(int));

    for(int i = 0; i < amount; i++)
    {
        printf("\n");
        printf("Enter the number %d:\n", i);
        while(scanf("%d", &array[i]) != 1)
        {
            printf("Re enter number: \n");
        }
    }

    printf("\n");

    funcRet = returnDuplic(array, amount);

    printf("These are the unnecessary repetitions:\n");
    for(int i = 0; i < funcRet.size; i++)
    {
        printf(" %d\n", funcRet.dups[i]);
    }

    return 0;
}

struct returnType returnDuplic(int *arrayPointer, int arrayLength)
{
    struct returnType ret;

    //the array 'ret.returnType' that will hold the duplicates and returned
    //it is declared as static so as to be able to be returned
    //memory will be allocated dynamically with each duplicate found

    ret.dups = (int *) malloc(1* sizeof(int)); //allocated memory enough for one duplicate

    int counter = 0; //counts the number of duplicates found

    int *this;
    int *last = arrayPointer + arrayLength - 1; //ex if the lenght of the array is 10, and the arrayPointer is at 0,
    //then the end is at 0+10-1, because the array starts at 0 and ends at 10

    for(this = arrayPointer+1; last > arrayPointer; arrayPointer++, this = arrayPointer + 1)
    {
        //ex in the first iteration, 'arrayPointer' will be pointing to the first element
        //in the array, and 'this' will be pointing to the second element.
        //This while loop will iterate 'this' from the second element to the last element,
        //and compare each's value (not pointer, hence the use of the *) with the value
        //pointed to by 'arrayPointer'.
        while(this <= last)
        {
            if(*this == *arrayPointer) //added to the array holding the duplicates
            {
                counter++;

                //on the first duplicate found, this will reset the memory to only
                //one int, because couter = 1.
                //Then on the second duplicate found, counter = 2, thus this will
                //allocate enough memory for two.
                ret.dups = realloc(ret.dups, (counter* sizeof(int)));

                ret.dups[counter-1] = *this;

                //say '1' appears 4 times in the array, and the indices at which it appears
                //are the following: 0, 3, 5, 8.
                //Thus when the while loop arrives at index 3, it will realise that '1' is
                //a duplicate, and it will add it to the array of duplicates.
                //So there's no need to continue looping searching for 3, because all
                //we need is to list the repeated integers, no need to state how many
                //times they are repeated.
                break;
            }

            this++;
        }

    }

    ret.size = counter;

    return ret;
}

