//Write a program that finds an approximation to the square root of a given
//number n using an iterative numerical method such as the Newton-
//Raphson Method.
// Created by Neil on 04/04/2017.
//

#include <math.h>
#include <stdio.h>

//the accuracy allows for a difference of 0.0001 between the approximated square root to the actual
float newtonRaph(int n);

int main(void)
{
    int num;
    int choice;

    printf("This program will approximate the square root of numbers that you enter, using the Newton-Raphson method.\n");

    printf("Enter: 1) To enter a number\n");
    printf("       2) To exit\n");
    printf("\n");

    while(((scanf("%d", &choice)) != 1) || ((choice < 1) || (choice > 3)))
    {
        printf("\n");
        printf("Re-enter a valid choice: ");
    }

    while(choice < 2)
    {
        printf("Please enter the number you want the square root of: \n");

        while((scanf("%d", &num) != 1) || (num < 0))
        {
            if (num < 0)
                printf("Negative numbers do not have a square root, re-enter a new number: \n");
            else
                printf("Re-enter a valid number: \n");
        }
        printf("\n");

        printf("The approximated square root of %d is: %f\n", num, newtonRaph(num));

        printf("\n");
        printf("Enter: 1) To enter a number\n");
        printf("       2) To exit\n");
        printf("\n");

        //keeps on asking for a choice, until the choice is within range, and until an integer value is entered
        while(((scanf("%d", &choice)) != 1) || ((choice < 1) || (choice > 3)))
        {
            printf("\n");
            printf("Re enter choice: \n");
        }
    }

    printf("Goodbye!\n");

    return 0;
}
//an iterative fucnction approximating the square root of a given number
float newtonRaph(int n)
{
    //taking the first guess to be always 1
    float approx = 1;

    //keeps on looping until the difference between the actual square root and the approximated one
    //is smaller than 0.0001.
    //The condition calculates the ratio of approx*approx/n, and then reduces 1.
    //so for example the ration is 1.393, then we educe 1, and it becomes 0.393,
    //thus we need to loop again.
    while(fabs((approx*approx / n) - 1) >= 0.0001)
    {
        //applying the newton raphson's method which calculates the next approximation by
        //averaging the old approximation with the quotient of the original number allover the old approximation
        approx = (approx + (n/approx)) / 2;
    }

    return approx;

}