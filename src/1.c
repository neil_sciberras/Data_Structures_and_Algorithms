//Write a program that, given a list of integers, finds all 2-pairs of integers
//that have the same product. A 2-pair is 2 distinct pairs of integers
//((a,b),(c,d)) where a X b = c X d and a ≠ b ≠ c ≠ d. The range of integers in
//the list should be from 1 to 1024.

// Created by Neil on 14/03/2017.

#include <stdio.h>
#include <stdlib.h>

struct product {
    int fac1;
    int fac2;
};

int main(void)
{
    int listLen = 0;
    int counter = 1; //counts the number of pairs, which is set to 1 intially
    struct product *pairs = (struct product *) malloc(counter * sizeof(struct product));

    printf("How many numbers would you like in the list?\n");
    while (scanf("%d", &listLen) != 1)
    {
        printf("Re enter a valid number: \n");
    }

    int list[listLen];

    for(int i = 0; i < listLen; i++)
    {
        list[i] = rand() % 1025;
    }

    //checks element by element against all the other elements in the array, for pairs that do not have equal operands
    for(int i = 0; i < listLen; i++)
    {
        for(int j = i+1; j < listLen; j++)
        {
            if(list[i] != list[j]) //if the pair is not 3x3 for example
            {
                pairs[counter-1].fac1 = list[i];
                pairs[counter-1].fac2 = list[j];

                counter++; //the counter is incremented

                pairs = realloc(pairs, (counter* sizeof(struct product))); //realloc does not add to the allocated
                //size to pairs, but defines the new allocated memory size
            }
        }
    }

    printf("\n");
    printf("These are the pairs whose products are equal such that the following constraint is held:\n");
    printf("\ta x b = c x d, such that a != b != c != d\n");
    printf("\n");

    //these will iterate 'pairs' and check for the operands condition:
    //for any a x b = c x d, a ≠ b ≠ c ≠ d
    for(int i = 0; i < counter; i++)
    {
        for(int j = i+1; j < counter; j++)
        {
            if((pairs[i].fac1*pairs[i].fac2) == (pairs[j].fac1*pairs[j].fac2))
            {
                if ((pairs[i].fac1 != pairs[j].fac1) && (pairs[i].fac1 != pairs[j].fac2) &&
                    (pairs[i].fac2 != pairs[j].fac1) && (pairs[i].fac2 != pairs[j].fac2)) {
                    printf("| %-4d x %4d = %8d |   <->   | %-4d x %4d = %8d |\n", pairs[i].fac1, pairs[i].fac2,
                           (pairs[i].fac1 * pairs[i].fac2), pairs[j].fac1, pairs[j].fac2,
                           (pairs[j].fac1 * pairs[j].fac2));
                }
            }
        }
    }

    return 0;

}


