// Write a program that accepts an input (a sequence of integers, input one by one)
// and then incrementally builds a Binary Search Tree (BST). There is no need to
// balance the BST.

// Created by Neil on 29/03/2017.

#include <stdio.h>
#include <stdlib.h>

struct node {
    int value;
    struct node* left;
    struct node* right;
};

void insert(struct node *sub_t_root, struct node *newNode); //a recursive function

struct node *createNode(int value);

void infix(struct node *subroot);

int main(void)
{
    int counter = 0; //counts the number of nodes entered in the BST

    int input; //the number entered by the user at every instance, will be stored in this variable

    int choice;

    struct node *root;

    struct node *temporary;

    printf("Choose: \n");
    printf(" 1) Enter a number in the BST\n");
    printf(" 2) Infix traversal of BST\n");
    printf(" 3) Exit\n");

    while(((scanf("%d", &choice)) != 1) || ((choice < 1) || (choice > 3)))
    {
        printf("\n");
        printf("Re-enter a valid choice: ");
    }

    printf("\n");

    while(choice < 3)
    {
        switch (choice)
        {
            case 1:
            {
                printf("Enter the number you want to enter in the BST:\n");

                while(scanf("%d", &input) != 1)
                {
                    printf("Re-enter:\n");
                }


                if (counter == 0) //when the BST is empty, we put the node being inserted as the root
                {
                     root = createNode(input);

                    counter++;

                }else //else search for the correct position to enter the inserted node
                {
                    temporary = createNode(input);

                    insert(root, temporary);
                }

                break;
            }

            case 2:
            {
                printf("Inifx traversal:(Left, Root, Right) ");
                infix(root);

                break;
            }


            default:
            {
                printf("The choice you entered is invalid.\n");
                printf("\n");

                break;
            }
        }

        //displaying all the options
        printf("\n");
        printf("Choose: \n");
        printf(" 1) Enter a number in the BST\n");
        printf(" 2) Infix traversal of BST\n");
        printf(" 3) Exit\n");
        printf("\n");

        //keeps on asking for a choice, until the choice is within range, and until an integer value is entered
        while(((scanf("%d", &choice)) != 1) || ((choice < 1) || (choice > 3)))
        {
            printf("\n");
            printf("Re enter choice: \n");
        }

        while(getchar() != '\n') //getting rid of the remaining characters in the buffer, because of the scanf, until the end of line is reached
            continue;
    }

    printf("Goodbye!\n");

    return 0;
}


struct node *createNode(int value)
{
    struct node *newNode;

    newNode = (struct node*) malloc(sizeof(struct node));

    newNode->value = value;
    newNode->left = NULL;
    newNode->right = NULL;

    return newNode;
}

void insert(struct node *sub_t_root, struct node *newNode)
{
    if(newNode->value < sub_t_root->value) //new node smaller than the root of the subtree
    {
        //if the subtree has no children on the left, then put the new node as the first
        //child of the root of the subtree
        if(sub_t_root->left == NULL)
        {
            sub_t_root->left = newNode;
        }else //else call insert recursively to do the same on the left child of the subtree
        {
            insert(sub_t_root->left, newNode);
        }
    }

    printf("\n");

    if(newNode->value > sub_t_root->value)
    {
        if(sub_t_root->right == NULL)
        {
            sub_t_root->right = newNode;
        }else
        {
            insert(sub_t_root->right, newNode);
        }
    }
}

void infix(struct node *subroot)
{
    //INFIX: left, root, right

    if(subroot != NULL)//checking whether the current node is empty
    {
        infix(subroot->left); //recursively calling infix() on the left subtree of the subroot
        printf("'%d'", subroot->value);
        infix(subroot->right); //recursively calling infix() on the right subtree
    }
}