//Write a program that uses an ADT Stack to evaluate arithmetic expressions in RPN
//format. The contents of the stack should be displayed on the screen during evaluation.
//The allowed arithmetic operators are +, -, x, /.

//The time complexity of this program is O(n).
//The size of a n expression is calculated by the number of operators and operands it has. So for example
//"1 2 3 + x" has size of 5.
//Now the code has a while loop that repeats for the number of tokens(one token per operand and operator),
//so it is a linear algorithm with time O(n).

// Created by Neil on 14/03/2017.

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAXSTACK 30
#define MAXEXP 100

struct stack
{
    int stk[MAXSTACK];
    int top;
};

typedef struct stack STACK;
STACK s;

void push(int  num);
int pop(void);
void display(void);

int main(void)
{
    s.top = -1; //this sets the stack pointer to -1, so when the first push is called, the stack pointer is incremented,
    //and it starts from location 0

    char expression[MAXEXP];
    char original[MAXEXP];
    char *token;

    int head;
    int bottom;

    printf("Enter an arithmetic expression in Reverse Polish Notation (Postfix): \n");
    fgets(expression, MAXEXP, stdin); //using the fgets that is usually used for input from file, but instead we're
    //using it for input from the 'standard input' aka keyboard.

    expression[strlen(expression)-1] = 0; //removes the last character in the expression
    //the last character is the new line character, because fgets reads that as a normal character

    strcpy(original, expression); //keeping a copy of the original expression, because the tokenize process will alter it

    token = strtok(expression, " ");

    push(atoi(token)); //the first token in the expression should always be a number, because
    //an RPN expression cannot start with an operator, so we push onto the stack right away

    display();
    printf("\n");

    while((token = strtok(NULL, " ")) != NULL)
    {

        if(*token == '+')
        {
            if(s.top >= 1)
            {
                push(pop() + pop());
                display();
                printf("\n");
            }else
            {
                printf("Sorry, you entered an invalid expression!\n");
                return 0;
            }

        }else if(*token == '-')
        {
            if(s.top >= 1) {
                head = pop();
                bottom = pop();

                push(bottom - head);

                display();
                printf("\n");
            }else
            {
                printf("Sorry, you entered an invalid expression!\n");
                return 0;
            }

        } else if(*token == 'x' || *token == 'X' || *token == '*')
        {
            if(s.top >= 1)
            {
                push(pop() * pop());
                display();
                printf("\n");
            }else
            {
                printf("Sorry, you entered an invalid expression!\n");
                return 0;
            }

        }else if(*token == '/')
        {
            if(s.top >= 1)
            {
                push(pop() / pop());

                display();
                printf("\n");
            }else
            {
                printf("Sorry, you entered an invalid expression!\n");
                return 0;
            }


        }else
        {
            push(atoi(token));
            display();
            printf("\n");
        }
    }

    printf("'%s' has a value of: %d\n", original, s.stk[0]);

    return 0;
}

void push(int  num)
{
    if(s.top == (MAXSTACK-1))
    {
        printf("Full stack\n");
        return;
    }else
    {
        s.top ++; //incrementing the stack pointer when pushing
        s.stk[s.top] = num;
    }
}

int pop(void)
{
    int num;

    if(s.top < 0)
    {
        printf("Empty stack\n");
        return (s.top);
    }else
    {
        num = s.stk[s.top];
        s.top --; //decrementing the stack pointer
    }

    return num;
}

void display(void)
{
    if(s.top < 0)
    {
        printf("\tEmpty stack!\n");
        return;
    }else
    {
        printf("\tStatus of stack:\n");
        for (int i = s.top; i >= 0; i--)
        {
            if(i == s.top)
            {
                printf("\t|  %d  | <- top\n", s.stk[i]);
            }else
            {
                printf("\t|  %d  |\n", s.stk[i]);
            }
        }
    }
}