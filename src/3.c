//Write a Boolean function that checks if a number is prime. Also
//implement the Sieve of Eratosthenes algorithm. Explain any optimizations
//made.

// Created by Neil on 20/03/2017.

#include <stdio.h>
#include <math.h>
#include <stdbool.h>

bool isPrime(int num);
int * sieve(int * list);

int main(void)
{
    /*testing the isPrime() function using numbers from 1 to 30*/
    printf("The prime numbers between 0 and 99, using a function that checks individual numbers' primality, are:\n");

    for(int i = 0; i < 100; i++)
    {
        if(isPrime(i) == true)
            printf("  %d is prime.\n", i);
    }





    /*testing the sieve() function using numbers from 2 to 100*/
    int wholeList[100];
    int *primes;

    for(int i = 0; i < 100; i++)
    {
        wholeList[i] = i;
    }

    primes = sieve(wholeList);

    printf("The prime numbers between 0 and 99, using the Sieve of Eratosthenes, are: \n");
    printf("\n");

    for(int i = 0; i < 100; i++)
    {
        if(primes[i] != 0)
            printf("  %d is prime.\n", primes[i]);
    }

    return 0;
}

bool isPrime(int num)
{
    bool prime = true;

    if((num == 2) || (num == 1))
        prime = true;
    else
    {
        if ((num % 2) == 0) // example 4%2 = 0, so 4 is not a prime number
            prime = false;
        else
        {
            int root = floor(sqrt(num));

            for(int i = 3; i <= root; i = i + 2)
            {
                if(num%i == 0)
                    prime = false;
            }
        }
    }

    return prime;

}

int * sieve(int * list)
{
    //For time complexity follow this link:
    //http://stackoverflow.com/questions/16472012/what-would-cause-an-algorithm-to-have-olog-log-n-complexity


    //this implementation of the sieve of eratosthenes is based on an array of length 100, containing values from 0 till 99

    //Marked elements will be represented by setting them to 0, and then in the end values that are 0s will not be
    //displayed in the final array of primes.

    //In this implementation of the sieve of Eratosthenes, 0 and 1 are not considered to be prime numbers.

    list[0] = 0;
    list[1] = 0;

    //this for loop will go from 2 in the list, till 10 which is the
    //square root of 100, for the same reason explained in the other function.
    for(int i = 2; i < floor(sqrt(100)); i++)
    {
        //this for loop will mark all the factors of the current prime we're at right now.
        //Something we noted was that for example when at prime number 3, we were iterating through 6
        //for nothing, because it was already marked when we were at 2. Similarly, when we were at
        //prime number 4, we were iterating through 8 for nothing, because it was already marked by 2.
        //So, an optimization was made, such that the factors of the current prime number start being marked
        //at the sqaure of the current prime number. So for example if we're at 6, we start marking its factors from 36,
        //not from 12, because from 12 till 36 would have already been marked by the previous prime numbers.
        for(int j = i*i; j < 100; j = j + i)
        {
            list[j] = 0;
        }
    }

    return list;
}