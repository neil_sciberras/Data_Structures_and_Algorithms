//Write a function that computes cosine or sine by taking the first n terms
//of the appropriate series expansion.

// Created by Neil on 11/04/2017.

#include <math.h>
#include <stdio.h>

#define PI 3.14159

int fact(int num);

float cosine(int n, float x);

float sine(int n, float x);

int main(void)
{
    float angleD;
    float angleR;
    int n;

    printf("Enter the angle in degrees:\n");

    while(scanf("%f", &angleD) != 1)
    {
        printf("Re-enter: \n");
    }

    angleR = angleD*PI/180;

    printf("%f degrees = %f radians.\n", angleD, angleR);
    printf("\n");

    printf("Enter 'n' which is the accuracy (number of elements in the series expansion):\n");

    while(scanf("%d", &n) != 1)
    {
        printf("Re-enter: \n");
    }

    printf("\n");
    printf("cos(%f) with accuracy of %d : %f\n", angleR, n, cosine(n, angleR));
    printf("cos(%f) as defined by math.h is : %f\n", angleR, cos(angleR));

    printf("\n");
    printf("sin(%f) with accuracy of %d : %f\n", angleR, n, sine(n, angleR));
    printf("sin(%f) as defined by math.h is : %f\n", angleR, sin(angleR));

}

int fact(int num)
{
    if(num <= 1)
        return 1;
    else
        return num*fact(num-1);
}

float cosine(int n, float x)
{
    //the angle must be in radians

    if(n == 0)
        return 1;
    else
    {
        if(n%2 == 0)
        {
            return (cosine(n-1, x) + (pow(x, 2*n) / (fact(2*n))));
        }else
        {
            return (cosine(n-1, x) + (((-1) * pow(x, 2*n)) / (fact(2*n))));
        }
    }
}

float sine(int n, float x)
{
    if(n == 0)
        return 0;
    else
    {
        if(n%2 == 0)
        {
            return (sine(n-1, x) + ((-1) * pow(x, ((2*n) - 1)) / (fact((2*n)-1))));
        }else
        {
            return (sine(n-1, x) + ((pow(x, ((2*n) - 1))) / (fact((2*n)-1))));
        }
    }

}