//Write a function that returns the sum of the first n numbers of the
//Fibonacci sequence (Wikipedia).

// Created by Neil on 11/04/2017.

#include <stdio.h>

int fibonacciSum(int n);

int main(void)
{
    int limit = 0;

    printf("Enter the limit:\n");
    while(scanf("%d", &limit) != 1)
    {
        printf("Re-enter\n");
    }

    printf("\n");
    printf("The sum of the first %d terms of the Fibonacci sequence (starting from 0) is: %d.\n", limit, fibonacciSum(limit));

    return 0;
}

int fibonacciSum(int n)
{
    int sum = 0;

    int num1 = -1;
    int num2 = 1;
    int num3 = 0;

    for(int i = 1; i <= n; i++)
    {
        num3 = num1 + num2;
        sum += num3;

        num1 = num2;
        num2 = num3;
    }

    return sum;
}